package unidad4;

import java.util.Scanner;

public class Ejer10_U4_Rto {

	public static void main(String[] args) {
		// Crear vector de numeros enteros de un tamaño especificado por usuario entre 10 y 20
		//Llenarlo con numeros aleatorios y mostrar el contenido del vector
				
		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca el tamaño del vector: ");
		int n = in.nextInt();
		
		while(n<10 || n>20) {
			System.out.println("El tamaño es incorrecto. Debe estar comprendido entre 10 y 20. \n Pruebe otra vez: ");
			n = in.nextInt();
		}
				
		int[] v = new int[n];
		
		for(int i=0; i<v.length; i++) {
			System.out.println(v[i] = (int) Math.round(Math.random() * Integer.MAX_VALUE));
			
		}

		System.out.println();
		
		int min = v[1] - v[0];
		int dif;
		
		for(int i=2; i<v.length; i++) {		
			dif = v[i] - v[i - 1];
			if(dif < min)
				min = dif;
		}
		
		System.out.println("Minima diferencia: " + min);
				
	}

	
//	public static void main(String[] args) {
//		// Crear vector de numeros enteros de un tamaño especificado por usuario entre 10 y 20
//		//Llenarlo con numeros aleatorios y mostrar el contenido del vector
//				
//		Scanner in = new Scanner(System.in);
//		System.out.println("Introduzca el tamaño del vector: ");
//		int n = in.nextInt();
//		
//		while(n<10 || n>20) {
//			System.out.println("El tamaño es incorrecto. Debe estar comprendido entre 10 y 20. \n Pruebe otra vez: ");
//			n = in.nextInt();
//		}
//				
//		int[] v = new int[n];
//		
//		for(int i=0; i<v.length; i++) {			
//			System.out.println(( (int) Math.round(Math.random() * Integer.MAX_VALUE) ) + " ");
//		}
//
//		System.out.println();
//		
//		int min = Integer.MAX_VALUE;
//		int dif;
//		
//		
//		for(int i=1; i<v.length; i++) {
//			dif = v[i] - v[i - 1];
//			if(dif < min)
//				min = dif;
//		}
//		
//		System.out.println("Minima diferencia: " + min);
//				
//	}

}
