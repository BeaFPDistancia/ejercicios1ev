package unidad4;

import java.util.Random;
import java.util.Scanner;

public class Ejer7_Unidad4 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		Random r = new Random();
		
		int vector [];
		
		System.out.println("Introduce la dimension del vector (10<=dim<=1.000.000):");
		int dim = in.nextInt();
		
		vector = new int[dim];
		long t0 = System.currentTimeMillis(); //para saber el tiempo que tardamos en llenar el vector
		
		
		
		for(int i=0; i<vector.length; i++) {
			int j;
			int n;
			do {
				n = r.nextInt(2000000) - 999999; //crea alatorios entre esos dos valores			
		  //vector[i] = r.nextInt(valormax - valormin +1) + valormin;			
				j=0;
				while(j < i && vector[j] != n) {
					j++;
				}
			}while(j != i);
			vector[i] = n;
		}
		
		System.out.println("Tamaño del vector: " + vector.length);
		
		long t1 = System.currentTimeMillis() - t0;
		System.out.println("Tiempo empleado en llenarlo " + t1 + " milisegundos");
		
		//para saber la diferencia entre los valores del vector y el tiempo empleado	
		t0 = System.currentTimeMillis(); 
		int dif = 0; 	
		dif = vector[dim-1]-vector[0];		
		System.out.println("Diferencia entre el valor mayor y el menor: " + dif);
		long t2 = System.currentTimeMillis() - t0;
		System.out.println("Tiempo empleado en calcular la diferencia de valores " + t2 + " milisegundos");
	}

}
