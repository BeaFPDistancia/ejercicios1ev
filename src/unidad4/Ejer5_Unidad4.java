package unidad4;

import java.util.Scanner;

public class Ejer5_Unidad4 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
		//char [] letras = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
				//System.out.println(letras);
		
		System.out.println(letras.charAt(11111111 % 23));
		
		System.out.println("Introduce un NIF: ");
		String nif = in.nextLine();
		
		//para obtener los n�meros del nif
		int numero = Integer.parseInt(nif.substring(0, nif.length() -1));
		
		//para obtener la letra del nif
		char letra = nif.charAt(nif.length() -1);
		
		int i = numero % 23;
		
		if(letra == letras.charAt(i))
			System.out.println("El nif es correcto");
		else
			System.out.println("El nif es incorrecto");
		

	}

}
