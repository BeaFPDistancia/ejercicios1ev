package unidad4;

import java.util.Scanner;

public class Ejer3_Unidad4 {

	public static void main(String[] args) {
		//lea dos cadenas desde el teclado y muestre el número de veces
		// que la segunda está contenida en la primera
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introduce dos cadenas: ");
		String cadena1 = in.nextLine();
		String cadena2 = in.nextLine();
		
		StringBuffer sb1 = new StringBuffer();
		StringBuffer sb2 = new StringBuffer();
		
		sb1.append(cadena1);
		sb2.append(cadena2);
		
		sb1.setLength(cadena1.length());	//longitud del StringBuffer la de la cadena1
		sb2.setLength(cadena2.length());	//longitud la de la cadena2
		
		System.out.println(sb1.capacity()); //capacidad memoria
		System.out.println(sb2.capacity());
		//System.out.println(sb1.substring(0,2)); //subcadena de 0 a 2 caracter
		
		
		System.out.println("La cadena2 está contenida " + sb1.compareTo(sb2) + " veces en la cadena1");
		}
		
	}

