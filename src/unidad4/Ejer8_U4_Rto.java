package unidad4;

import java.util.Scanner;

public class Ejer8_U4_Rto {

	public static void main(String[] args) {
		// Crear vector de num enteros, tamaño especificado por usuario, entre 100 y 200

		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca el tamaño del vector, (debe ser un número entre 10 y 200): ");
		int n = in.nextInt();
				
		while(n<10 || n>200) {
			System.out.println("Incorrecto. El numero debe estar entre 10 y 200. \n Prueba otra vez: " );
			n = in.nextInt();
		}
		in.close();
				
															
		int[] v = new int[n]; 
				
		//llena el vector con numeros aleatorios entre -100 y 100		 
		for(int i=0; i<v.length; i++) {			
			
			v[i] = (int) (Math.round(Math.random() * 200) - 100);				
		}		

		//mostrar suma de numeros almacenados, pero si en alguna posición se encuentra almacenado el 13
		//no se sumará ni éste ni las 13 posiciones siguientes
		//si la suma de todos ellos es distinta de 7
		
			int suma = 0;
			int parcial = 0;
			int nosumar = 0; 	//posicion del vector
			int nosumados = 0;
			
			for(int i=0; i<v.length; i++) {
				if(v[i] == 13 && nosumar == 0) {
					nosumar = 14;
					parcial = 0;
					System.out.println("{ ");
				}
			
				System.out.println(v[i] + " ");
				
				if(nosumar == 0)
					suma += v[i];
				else {
						parcial += v[i];
						if(--nosumar == 0) {
							if(parcial == 7)
								suma += parcial;
							else
								nosumados += 14;
							System.out.println("} ");								
						}
				}
			}
			System.out.println();
			if(nosumar > 0) {
				if(parcial == 7)
					suma += parcial;
				else
					nosumados = 14 - nosumar;
			}
			
			System.out.println("Suma: " + suma);
			System.out.println("No sumados: " + nosumados);
		}
}
