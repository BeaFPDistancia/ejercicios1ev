package unidad4;

import java.util.Arrays;
import java.util.Scanner;

public class Ejer9_Unidad4 {


	public static void main(String[] args) {
		// Crear vector de numeros enteros de un tamaño aleatorio entre 10 y 500 elementos
		
		Scanner in = new Scanner(System.in); // para comprobar resultados
		int n = in.nextInt();
								//int n = (int) Math.round(Math.random()*490 + 10); aleatorios entre 10 y 500

		while(n<10 || n>500){
			System.out.println("El vector no tiene el tamaño correcto. Debe tener un tamaño ente 10 y 500 elementos. \n Prueba a definirlo otra vez.");
			n = in.nextInt();
		}
		in.close();
		
		int[] vector = new int[n];	
		
		//int[] vector = {2,2,3,3,3}; para comprobar que funciona
		
		for(int i=1; i<vector.length; i++) {
			
			vector[i] = (int) Math.round(Math.random()*200 - 100); //entre -100 y 100 aleatorio;
		}		
			if(vector.length <= 50)
				System.out.println("El contenido del vector es: " + Arrays.toString(vector));
			
		
		
		System.out.println("El numero de secuencias de números repetidos es: " + secuenciasDeNumerosRepetidos(vector));
	}
		
		
		static int secuenciasDeNumerosRepetidos(int[] vector) {
			int secuencias = 0;
			boolean serepite = false;
			
			for(int i=1; i<vector.length; i++) {
				if(vector[i] == vector[i - 1]) {
					if(!serepite) {
						serepite = true;
						secuencias++;
					}
				}else if (serepite)
					serepite = false;
			}
			return secuencias;
		}		
	

}
