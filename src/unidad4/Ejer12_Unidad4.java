package unidad4;

public class Ejer12_Unidad4 {

	public static void main(String[] args) {
		int[] v = {6, 2, 3, 0, 1};
		//int[] v = {1, 2, 1, 1};
		Integer c = centro(v);
		System.out.println(c != null ? ("El centro está en la posición " + c) : "No tiene centro");
	}
	
	static Integer centro(int [] vector) {
		int c = 0;
		//int c = 1; comenzamos la busqueda en el indice 1
		int izqda;
		int dcha;
		
		do { //para hacer el recorrido una vez por lo menos
			
			c++; //para return c
			izqda = dcha = 0;
			
			for(int i=0; i<c; i++) {
				izqda += (c - i) * vector[i];										
			}
			for(int j=c+1; j<vector.length; j++) {
				dcha += (j - c) * vector[j];								
			}	
			
		}while(izqda != dcha && c <= vector.length-1); //finaliza si (izqda == dcha) y c <= vector.length -1 para return c		
		//}while(izqda != dcha && c < vector.length-1);	//y c < vector.length -1 para return c-1
		
		if(c == vector.length -1)
			return null;
		else
			return c;
			//return c - 1;
	}

}
