package unidad4;

import java.util.Scanner;

public class Ejer2_Unidad4 {

	public static void main(String[] args) {
		// Leer una cadena y mostrarla invertida
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introduce una palabra: ");
		String palabra = in.nextLine();
		
		StringBuffer sb = new StringBuffer();
		sb.append(palabra);
		
		//sb.reverse(); metodo para mostrar la cadena de caracteres invertida
		System.out.println(sb.reverse());
	}

}
