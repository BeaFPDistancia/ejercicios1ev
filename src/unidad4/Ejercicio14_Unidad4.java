package unidad4;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio14_Unidad4 {

	public static void main(String[] args) {
		
		int [][] numeros; //se puede ver como un vector aunque está declarado como un array de 2 dimensiones
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Numero de secuencias: ");
		//int n = in.nextInt(); para introducir el número de secuencias
		
		//numeros = new int[n][]; para definir el vector de n elementos
		
		numeros = new int[in.nextInt()][]; //todo en una línea
		
		for(int i=0; i<numeros.length; i++) {
			System.out.println("Cantidad de elementos de la secuencia " + (i + 1) + ": ");
			numeros[i] = new int[in.nextInt()];
			System.out.println("Introduce los números de la secuencia: ");
			for(int j=0; j<numeros[i].length; j++) {
				numeros[i][j] = in.nextInt();
				
			}
		}
		
		for(int[] v: numeros) {
			for(int n: v)
				System.out.printf("%-5d",n);//modificando el print por un formato
			System.out.println();
		}
		
		for (int[] v: numeros)
			System.out.println(Arrays.toString(v));
	}

}
