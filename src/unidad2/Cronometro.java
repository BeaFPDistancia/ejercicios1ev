package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cronometro {

	public static void main(String[] args) throws IOException {
		// Introducir el nombre
		String nombre; 
		
		//sin utilizar la clase Scanner
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		//medida inicial
		long t0 = System.currentTimeMillis();
		System.out.println("Dime tu nombre: ");
		nombre = in.readLine();		
		
		//medida final
		long t1 = System.currentTimeMillis() - t0;
		//medida en segundos
		long segundos = t1 / 1000;
		//resto, medida en milisegundos
		long miliseg = t1 % 1000;
		
		System.out.printf("Hola %s, has tardado %d,%3d segundos ", nombre, segundos, miliseg);
		

	}

}
