package unidad2;

import java.util.Scanner;

public class Sueldo {

	public static void main(String[] args) {
		String sueldoBase;
		double sueldoTotal;
		double porcentaje;
		double comision;		
		int ventas;
		
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduzca el importe de su sueldo base: ");
		sueldoBase = teclado.nextLine();
		porcentaje = 0.10f;
		comision = Double.parseDouble(sueldoBase)*porcentaje;		
		ventas = 3;
		sueldoTotal = Double.parseDouble(sueldoBase) + comision*ventas;
		
		System.out.printf("Le corresponde en concepto de comisi�n por cada venta : %-15.02f ",  comision);
		System.out.println("El sueldo total que le corresponde por las " + ventas + " ventas realizadas es: ");
		System.out.printf("Sueldo total: %-15.2f ", sueldoTotal);

	}

}
