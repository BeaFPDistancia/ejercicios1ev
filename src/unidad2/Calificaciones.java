package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calificaciones {

	public static void main(String[] args) throws NumberFormatException, IOException {
		//calificaciones de examen y tareas de mates, fisica y quimica
		int examMat, tarea1Mat, tarea2Mat, tarea3Mat;
		int examFis, tarea1Fis, tarea2Fis;
		int examQui, tarea1Qui, tarea2Qui, tarea3Qui;
		float promedioMat, promedioFis, promedioQui, promedio;
		
		//sin utilizar la clase Scanner
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		//--- Matematicas ---
		System.out.println("Nota del examen de matem�ticas: ");
		//para leer la nota convertimos el string en int
		examMat = Integer.parseInt(in.readLine());
		System.out.println("Nota de la primera tarea de Matem�ticas: ");
		tarea1Mat = Integer.parseInt(in.readLine());
		System.out.println("Nota de la segunda tarea de Matem�ticas: ");
		tarea2Mat = Integer.parseInt(in.readLine());
		System.out.println("Nota de la tercera tarea de Matem�ticas: ");
		tarea3Mat =Integer.parseInt(in.readLine());
		//cast 
		promedioMat = (float) (examMat * 0.9 + ((tarea1Mat + tarea2Mat + tarea3Mat)/3) * 0.1);
		
		
		//--- Fisica ---
		System.out.println("Nota del examen de F�sica: ");
		examFis = Integer.parseInt(in.readLine());
		System.out.println("Nota de la primera tarea de F�sica: ");
		tarea1Fis = Integer.parseInt(in.readLine());
		System.out.println("Nota de la segunda tarea de F�sica: ");
		tarea2Fis = Integer.parseInt(in.readLine());
		promedioFis = (float) (examFis * 0.8 + ((tarea1Fis + tarea2Fis)/2) * 0.2);
		
		
		//--- Quimica ----
		System.out.println("Nota del examen de Qu�mica: ");
		examQui = Integer.parseInt(in.readLine());
		System.out.println("Nota de la primera tarea de Qu�mica: ");
		tarea1Qui = Integer.parseInt(in.readLine());
		System.out.println("Nota de la segunda tarea de Qu�mica: ");
		tarea2Qui = Integer.parseInt(in.readLine());
		System.out.println("Nota de la tercera tarea de Qu�mica: ");
		tarea3Qui = Integer.parseInt(in.readLine());		
		promedioQui = (float) (examQui * 0.85 + ((tarea1Qui + tarea2Qui + tarea3Qui)/3) * 0.15);
		
		
		promedio = (promedioMat + promedioFis + promedioQui)/3;
		
		System.out.printf("El promedio para Matem�ticas es: %4.2f\n" , promedioMat);
		System.out.printf(" El promedio para F�sica es: %4.2f\n" , promedioFis);
		System.out.printf("El promedio para Qu�mica es: %4.2f\n," , promedioQui);
		System.out.printf("El promedio general es: %4.2f\n" , promedio);
	}

}
