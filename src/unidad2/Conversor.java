package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor {

	public static void main(String[] args) throws IOException {
		//euros = 1.17 dolares
		float eur;
		float dolar;
		String linea;
		
		//alternativa a clase Scanner
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Introduce un n�mero: ");
		//linea = in.readLine();
		
		//para hacer la conversion
		eur = Float.parseFloat(in.readLine());
		dolar = eur * 1.17f;
		System.out.printf("%-15.2f", dolar);
		
		//System.out.println("La cantidad " + linea + " euros son: " );
		//System.out.printf("%-15.2f dolares",dolar);

	}

}
