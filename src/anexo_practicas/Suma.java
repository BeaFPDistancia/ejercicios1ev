package anexo_practicas;

import java.io.IOException;
import java.util.Scanner;

public class Suma {
	 
	public static void main(String[] args) throws IOException {
		//suponemos dos numeros enteros		
		int num1 = 5; 
		int num2 = 4;
		int suma;
		
		try (// Introducir dos n�meros por teclado y obtener la suma
		Scanner teclado = new Scanner(System.in)) {
			System.out.println("Introduce un n�mero entero:");
			num1 = teclado.nextInt();
			
			System.out.println("Introduce un n�mero entero:");
			num2 = teclado.nextInt();
		}

			//La suma se muestra por pantalla		
	suma = num1 + num2;
	System.out.println("La suma de los n�meros es:" + suma);
	
	//La resta se muestra por pantalla (suma de un n� negativo)		
			int resta = num1 - num2;
			System.out.println("La resta de los n�meros es:" + resta);
			
	
	}//cierra main

}//cierra class Suma
