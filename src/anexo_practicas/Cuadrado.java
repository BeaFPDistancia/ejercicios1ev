package anexo_practicas;

import java.util.Scanner;

public class Cuadrado {

	public static void main(String[] args) {
		//Calcular el cuadrado de un n�mero
		int num;
		try (Scanner teclado = new Scanner(System.in)) {
			System.out.println("Introduce un n�mero: ");
			num = teclado.nextInt();
		}
		
		int cuadrado = num * num;
		System.out.println("El cuadrado del n�mero que has introducido " + num + " es: " + cuadrado);
		
		

	}

}
