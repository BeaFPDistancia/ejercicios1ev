package unidad3;

import java.util.Random;
import java.util.Scanner;

public class AdivinaNumero {

	public static void main(String[] args) {
		
		Random num_aleatorio = new Random();
		int N = num_aleatorio.nextInt(100000) + 1000;
		int num;
		
		Scanner in = new Scanner(System.in);
		
		do {
			System.out.println("He pensado un n�mero adivina c�al es: ");
			num = in.nextInt();
	
			if(num > N)
				System.out.println("El numero introducido "+ num +" es mayor que el he pensado, prueba otra vez");
			else
				System.out.println("El numero introducido "+ num +" es menor que el he pensado, prueba otra vez");
			
		}while(num != N);
		
	}

}
