package unidad3;

import java.util.Scanner;

public class Multiplicar {
	

	public static void main(String[] args) {
		// comprobar conocimiento de las tablas de multiplicar
		
		solucionA();	
	}
	
	static Scanner in = new Scanner(System.in);
	
	static String deseaContinuar(String pregunta) {
		System.out.println(pregunta);
		String respuesta;
		respuesta = in.nextLine();		
		return respuesta;
	}

	static void solucionA() {
		
		int tabla;
		int resultado;
		int resultadoCorrecto;
		int fallos;
		
		do{
		
			System.out.println("Introduzca el número de la tabla de multiplicar");
			tabla = Integer.parseInt(in.nextLine());
			fallos = 0;
			
			for(int i=0; i<=9; i++) {
				
				resultadoCorrecto = tabla * i;
				System.out.println(tabla + " x " + i + "?");
				resultado = Integer.parseInt(in.nextLine());
				if(resultado == resultadoCorrecto) {
					System.out.println("Correcto");
				}
				else{
					fallos++;
					System.out.println("Incorrecto. Suma un fallo");
				}
			}if(fallos < 2) {
				System.out.println("Has aprobado");
			}	
			else {
				System.out.println("Has suspendido");
			}

		}while(deseaContinuar("¿Desea repasar otra tabla de multiplicar (s/n)?").equalsIgnoreCase("s"));
				
		
		
	}	
}
