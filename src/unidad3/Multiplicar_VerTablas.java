package unidad3;

import java.util.Scanner;

public class Multiplicar_VerTablas {

	public static void main(String[] args) {
		// Repasar las tablas de multiplicar del 1 a 9
		
		Scanner sc = new Scanner(System.in);

		//para repasar al menos una vez todas las tablas
		while(verTablas("¿Desea ver todas las tablas de multiplicar (s/n)? ").equalsIgnoreCase("s")){	
			
			for(int i=1; i<=10; i++) {
				for(int j=1; j<=10; j++) {
					int tabla = i*j; 
						
					System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + tabla);
				}
			}
			
		}

		while(verTablas("¿Desea repasar una tabla de multiplicar (s/n)? ").equalsIgnoreCase("s")){	//Desea repasar otra
			
			System.out.println("Introduzca el número de la tabla (entre el 1 y el 9)");
			String numero = sc.nextLine();
			
			if (Integer.valueOf(numero) == 1) {
				for(int i = 1; i<=10;) {
					for(int j = 1; j<=10; j++)
					System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					break;
					
				}
			}
			else if (Integer.valueOf(numero) == 2) {
				for(int i = 2; i<=10; ) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);						
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 3) {
				for(int i = 3; i<=10;) {
					for(int j = 1; j<=10; j++) {					
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 4) {
				for(int i = 4; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}		
			}
			else if (Integer.valueOf(numero) == 5) {
				for(int i = 5; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 6) {
				for(int i = 6; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 7) {
				for(int i = 7; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 8) {
				for(int i = 8; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}
			else if (Integer.valueOf(numero) == 9) {
				for(int i = 9; i<=10;) {
					for(int j = 1; j<=10; j++) {
						System.out.println("La tabla del " + i + " es: " + i + "x" + j + "=" + i*j);
					}
					break;
				}
			}else {
				
			}
			
		}

	}

	static String verTablas(String pregunta) {
		Scanner sc = new Scanner(System.in);
		System.out.println(pregunta);
		String respuesta = sc.nextLine();
		return respuesta;
		
	}

}
