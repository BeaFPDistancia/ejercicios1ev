package unidad3;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
								
		Scanner in = new Scanner(System.in);
		
		System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
		
		
		for(int operacion = 0; operacion<=4; operacion++) {
			operacion = in.nextInt();
			if((operacion == 0)&&(operacion!=1)&&(operacion!=2)&&(operacion!=3)&&(operacion!=4))
				System.out.println("Ha escogido suma.");
			else if((operacion == 1)&&(operacion != 0)&&(operacion!=2)&&(operacion!=3)&&(operacion!=4))
				System.out.println("Ha escogido resta.");
			else if((operacion == 2)&&(operacion!=0)&&(operacion!=1)&&(operacion!=3)&&(operacion!=4))
				System.out.println("Ha escogido multiplicación.");
			else if((operacion == 3)&&(operacion!=0)&&(operacion!=1)&&(operacion!=2)&&(operacion!=4))
				System.out.println("Ha escogido división.");
			else
				System.out.println("Ha escogido apagar.");
				
			int numero1;
			int numero2;
			int resultado;	
					
			switch (operacion) {
			case 0:
				System.out.println("Introduzca dos números: ");
				numero1 = in.nextInt();
				numero2 = in.nextInt();		
				resultado = numero1 + numero2;
				System.out.println(numero1 + " + " + numero2 + " = " + resultado);
				System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
				continue;
				
			case 1:
				System.out.println("Introduzca dos números: ");
				numero1 = in.nextInt();
				numero2 = in.nextInt();
				resultado = numero1 - numero2;
				System.out.println(numero1 + " - " + numero2 + " = " + resultado);
				System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
				continue;
				
			case 2:
				System.out.println("Introduzca dos números: ");
				numero1 = in.nextInt();
				numero2 = in.nextInt();
				resultado = numero1 * numero2;
				System.out.println(numero1 + " x " + numero2 + " = " + resultado);
				System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
				continue;
				
			case 3:
				System.out.println("Introduzca dos números: ");
				numero1 = in.nextInt();
				numero2 = in.nextInt();
				resultado = numero1 / numero2;
				System.out.println(numero1 + " / " + numero2 + " = " + resultado);
				
				System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
				continue;
				
			case 4:
				System.out.println("Apagar y salir");
				break;
			default:
				System.out.println("Eliga una operación: Suma-->0, Resta-->1, Multiplicación-->2, División-->3, Apagar-->4 ");
				
			}
		}
		in.close();
		
	}
	
}
