package unidad3;

import java.util.Scanner;

public class RetratoRobot {
	
	static Scanner in = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		
		String[][] rasgos = {
			{"WWWWWWWWW", "\\\\\\//////", "|\"\"\"\"\"\"\"|", "|||||||||"},
			{"| o  o |", "|-(� �)-|", "|-(o o)|", "| \\  / |" },
			{"@   J   @", "{   \"  }", "[  j   ]", "<  -  >" },
			{"|  === |", "|  -  |", "| ___ |", "| --- |"},
			{"\\______/", "\\,,,,,,/"}
				
		};
		
		
		int pelo = elegirRasgo(rasgos[0], "el pelo");
		int ojos = elegirRasgo(rasgos[1], "los ojos");
		int nariz = elegirRasgo(rasgos[2], "nariz");
		int boca = elegirRasgo(rasgos[3], "boca");
		int barbilla = elegirRasgo(rasgos[4], "barbilla");
		
		System.out.println("El retrato que has creado es: ");
		System.out.println(rasgos[0][pelo]);
		System.out.println(rasgos[1][ojos]);
		System.out.println(rasgos[2][nariz]);
		System.out.println(rasgos[3][boca]);
		System.out.println(rasgos[4][barbilla]);
	}
	
	static int elegirRasgo(String [] rasgos, String faccion){
			int opcion;
			do {
				System.out.println("Elige un rasgo para " + faccion);
				
				for(int i=0; i<rasgos.length; i++)
					System.out.println((i + 1) + " - " + rasgos[i]);
				opcion = in.nextInt();
				
				if(opcion < 1 || opcion > rasgos.length)
					System.out.println("Opcion incorrecta");
				
				}while(opcion < 1 || opcion > rasgos.length);
			
			return opcion -1;
		}
		
	

}
