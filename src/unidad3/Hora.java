package unidad3;

import java.util.Scanner;

public class Hora {

	public static void main(String[] args) {

		int h;
		int m;
		int s;
		
		Scanner teclado = new Scanner(System.in);
		//23:45:59 --> 23:45:00 comprobar que pasa de 59 a 60
		
		System.out.println("Hora: ");
		h = teclado.nextInt();
		System.out.println("Minutos: ");
		m = teclado.nextInt();
		System.out.println("Segundos: ");
		s = teclado.nextInt();

		//incrementamos en una unidad los segundos s=s+1
		s++;  
		if(s == 60) {
			s = 0;
			m++;
			if( m == 60) {
				m = 0;
				h++;
				if(h == 23)
					h = 0;
			}
		}
		System.out.println("La hora correspondiente al minuto siguiente es: " + h + ":" + m + ":" + s);
	}

}
